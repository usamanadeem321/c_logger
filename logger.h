#ifndef LOGGER_H
#define LOGGER_H

#include <stdio.h>
#include <time.h>

/* Default level */
#ifndef LOG_LEVEL
    #define LOG_LEVEL   WARNING
#endif

/* Colour customization */
#define DEBUG_COLOUR    ""
#define INFO_COLOUR     "\x1B[36m"
#define NOTICE_COLOUR   "\x1B[32;1m"
#define WARNING_COLOUR  "\x1B[33m"
#define ERROR_COLOUR    "\x1B[31m"
#define CRITICAL_COLOUR "\x1B[41;1m"


#ifndef COMPILATION_TIME_LOGGING_LEVEL
    #define COMPILATION_TIME_LOGGING_LEVEL DEBUG
#endif

/* Do not change this. */
#define RESET_COLOUR    "\x1B[0m"

/* Formatting prefs. */
#define MSG_ENDING      "\n"
#define TIME_FORMAT     "%T"
#define BORDER          "-"

/* Enabler flags */
#define DISPLAY_COLOUR  1
#define DISPLAY_TIME    1
#define DISPLAY_LEVEL   1
#define DISPLAY_FUNC    1
#define DISPLAY_FILE    1
#define DISPLAY_LINE    1
#define DISPLAY_BORDER  1
#define DISPLAY_MESSAGE 1
#define DISPLAY_ENDING  1
#define DISPLAY_RESET   1

#define LOG_PRINT 1
#define BOTH 1
#define LOGFILE 2
#define CONSOLE 3

FILE *fptr;

/* Log to log file */
#if LOG_PRINT == LOGFILE
#define emit_log(colour, level, file, func, line, ...) do {                 \
    /* notate the time */                                                           \
    time_t raw_time = time(NULL);                                                   \
    char time_buffer[80];                                                           \
    strftime(time_buffer, 80, TIME_FORMAT, localtime(&raw_time));                   \
                                 \
    /* display the time */                                                          \
    fprintf(fptr,"%s%s", DISPLAY_TIME ? time_buffer : "", DISPLAY_TIME ? " " : ""); \
                                                                                    \
    fprintf(fptr,"%10s%s", DISPLAY_LEVEL ? level : "", DISPLAY_LEVEL ? " " : "");   \
                                                                                    \
    fprintf(fptr,"%s%s", DISPLAY_FUNC ? func : "", DISPLAY_FUNC ? " " : "");              \
    \
    /* display the file and/or the line number */                                   \
    fprintf(fptr,                                                                         \
        "%s%s%s%.d%s%s",                                                            \
        DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? "(" : "",                  \
        DISPLAY_FILE ? file : "",                                                   \
        DISPLAY_FILE && DISPLAY_LINE ? ":" : "",                                    \
        DISPLAY_LINE ? line : 0,                                                    \
        DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? ") " : "",                 \
        !DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? " " : ""                  \
    );        \
       /* display message border */                                                    \
    fprintf(fptr,"%s%s", DISPLAY_BORDER ? BORDER : "", DISPLAY_BORDER ? " " : "");        \
\
    /* display the callee's message */                                              \
    if (DISPLAY_MESSAGE) fprintf(fptr,__VA_ARGS__);                                       \
    /* add the message ending (usually '\n') */                                     \
    fprintf(fptr,"%s", DISPLAY_ENDING ? MSG_ENDING : "");                                 \
} while (0)
#endif

/* Log to screen */
#if LOG_PRINT == BOTH
#define emit_log(colour, level, file, func, line, ...) do {                 \
    /* notate the time */                                                           \
    time_t raw_time = time(NULL);                                                   \
    char time_buffer[80];                                                           \
    strftime(time_buffer, 80, TIME_FORMAT, localtime(&raw_time));                   \
                                 \
    /* display the time */                                                          \
    printf("%s%s", DISPLAY_TIME ? time_buffer : "", DISPLAY_TIME ? " " : "");       \
    fprintf(fptr,"%s%s", DISPLAY_TIME ? time_buffer : "", DISPLAY_TIME ? " " : ""); \
                                                                                    \
    fprintf(fptr,"%10s%s", DISPLAY_LEVEL ? level : "", DISPLAY_LEVEL ? " " : "");   \
    printf("%10s%s", DISPLAY_LEVEL ? level : "", DISPLAY_LEVEL ? " " : "");   \
                                                                                    \
    fprintf(fptr,"%s%s", DISPLAY_FUNC ? func : "", DISPLAY_FUNC ? " " : "");              \
    printf("%s%s", DISPLAY_FUNC ? func : "", DISPLAY_FUNC ? " " : "");              \
    \
    /* display the file and/or the line number */                                   \
    fprintf(fptr,                                                                         \
        "%s%s%s%.d%s%s",                                                            \
        DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? "(" : "",                  \
        DISPLAY_FILE ? file : "",                                                   \
        DISPLAY_FILE && DISPLAY_LINE ? ":" : "",                                    \
        DISPLAY_LINE ? line : 0,                                                    \
        DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? ") " : "",                 \
        !DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? " " : ""                  \
    );        \
    printf(                                                                         \
        "%s%s%s%.d%s%s",                                                            \
        DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? "(" : "",                  \
        DISPLAY_FILE ? file : "",                                                   \
        DISPLAY_FILE && DISPLAY_LINE ? ":" : "",                                    \
        DISPLAY_LINE ? line : 0,                                                    \
        DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? ") " : "",                 \
        !DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? " " : ""                  \
    );        \
       /* display message border */                                                    \
    fprintf(fptr,"%s%s", DISPLAY_BORDER ? BORDER : "", DISPLAY_BORDER ? " " : "");        \
    printf("%s%s", DISPLAY_BORDER ? BORDER : "", DISPLAY_BORDER ? " " : "");        \
\
    /* display the callee's message */                                              \
    if (DISPLAY_MESSAGE) fprintf(fptr,__VA_ARGS__);                                       \
    if (DISPLAY_MESSAGE) printf(__VA_ARGS__);                                                                                \
    /* add the message ending (usually '\n') */                                     \
    fprintf(fptr,"%s", DISPLAY_ENDING ? MSG_ENDING : "");                                 \
    printf("%s", DISPLAY_ENDING ? MSG_ENDING : "");                                \
} while (0)
#endif

/* Log to screen */
#if LOG_PRINT == CONSOLE
#define emit_log(colour, level, file, func, line, ...) do {                         \
                                                                                 \
    /* notate the time */                                                           \
    time_t raw_time = time(NULL);                                                   \
    char time_buffer[80];                                                           \
    strftime(time_buffer, 80, TIME_FORMAT, localtime(&raw_time));                   \
                                                                                    \
    /* display the time */                                                          \
    printf("%s%s", DISPLAY_TIME ? time_buffer : "", DISPLAY_TIME ? " " : "");       \
                                                                                    \
    /* display the level */                                                         \
    printf("%10s%s", DISPLAY_LEVEL ? level : "", DISPLAY_LEVEL ? " " : "");         \
                                                                                    \
    /* display the function doing the logging */                                    \
    printf("%s%s", DISPLAY_FUNC ? func : "", DISPLAY_FUNC ? " " : "");              \
                                                                                    \
    /* display the file and/or the line number */                                   \
    printf(                                                                         \
        "%s%s%s%.d%s%s",                                                            \
        DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? "(" : "",                  \
        DISPLAY_FILE ? file : "",                                                   \
        DISPLAY_FILE && DISPLAY_LINE ? ":" : "",                                    \
        DISPLAY_LINE ? line : 0,                                                    \
        DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? ") " : "",                 \
        !DISPLAY_FUNC && (DISPLAY_FILE || DISPLAY_LINE) ? " " : ""                  \
    );                                                                              \
                                                                                    \
    /* display message border */                                                    \
    printf("%s%s", DISPLAY_BORDER ? BORDER : "", DISPLAY_BORDER ? " " : "");        \
                                                                                    \
    /* display the callee's message */                                              \
    if (DISPLAY_MESSAGE) printf(__VA_ARGS__);                                       \
                                                                                    \
    /* add the message ending (usually '\n') */                                     \
    printf("%s", DISPLAY_ENDING ? MSG_ENDING : "");                                 \
                                                                                    \
} while (0)
#endif


/* Level enum */
#define DEBUG       0
#define INFO        1
#define NOTICE      2
#define WARNING     3
#define ERROR       4
#define CRITICAL    5
#define SILENT      6

/* DEBUG LOG */
#if COMPILATION_TIME_LOGGING_LEVEL <= DEBUG
#define debug(...) do {                                                            \
    if (LOG_LEVEL == DEBUG) {                                                       \
        emit_log(                                                                   \
            DEBUG_COLOUR, "[DEBUG]", __FILE__, __func__, __LINE__, __VA_ARGS__      \
        );                                                                          \
    }                                                                               \
} while (0)
#else
#define debug(...)
#endif

/* INFO LOG */
#if COMPILATION_TIME_LOGGING_LEVEL <= INFO
#define info(...) do {                                                            \
        emit_log(                                                                   \
            INFO_COLOUR, "[INFO]", __FILE__, __func__, __LINE__, __VA_ARGS__        \
        );                                                                             \
} while (0)
#else
#define info(...)
#endif

/* NOTICE LOG */
#if COMPILATION_TIME_LOGGING_LEVEL <= NOTICE
#define notice(...) do {                                              \
        emit_log(                                                                   \
            NOTICE_COLOUR, "[NOTICE]", __FILE__, __func__, __LINE__, __VA_ARGS__    \
        );                                                                          \
                                                                            \
} while (0)
#else
#define notice(...)
#endif


/* WARNING LOG */
#if COMPILATION_TIME_LOGGING_LEVEL <= WARNING
#define warning(...) do {                                                           \
        emit_log(                                                                   \
            WARNING_COLOUR, "[WARNING]", __FILE__, __func__, __LINE__, __VA_ARGS__  \
        );                                                                          \
} while (0)
#else
#define warning(...)
#endif


/* ERROR LOG */
#if COMPILATION_TIME_LOGGING_LEVEL <= ERROR
#define error(...) do {                                                             \
        emit_log(                                                                   \
            ERROR_COLOUR, "[ERROR]", __FILE__, __func__, __LINE__, __VA_ARGS__      \
        );\
} while (0)
#else
#define error(...)
#endif

/* CRITICAL LOG */
#if COMPILATION_TIME_LOGGING_LEVEL <= CRITICAL
#define critical(...) do {                                                     \
        emit_log(                                                                   \
            CRITICAL_COLOUR, "[CRITICAL]", __FILE__, __func__, __LINE__, __VA_ARGS__\
        );                                                                          \
\
} while (0)
#else
#define critical(...)
#endif




#endif // logger.h