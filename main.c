#define LOG_LEVEL  0

#include "logger.h"
#include <stdio.h>

#define assert_int(x, y) do {   \
    if (x != y) \
        critical("%d and %d are not equal!", x, y); \
} while (0)

void fake_allocate(char *buf)
{
    if (!buf) {
        critical("Non-NULL argument expected");
        return;
    }

    *buf = 'D';
}

int log_file_init (){

    fptr = fopen("logs.txt", "w");
     if (fptr == NULL) {
        printf("Error!");
        return 0;
    }

}

int main(void)
{
    // Smoke testing
    log_file_init();
    debug("This is a debug log %d", 2);
    info("This is an informational log");
    notice("notice log");
    warning("warning log");
    error("error log");
    critical("critical log");


    info("declaring int 5 and int 6");
    int first = 5;
    int second = 6;
    assert_int(first, second);
    puts("");


    // Self-diagnostic indicators
    debug("your logging level is DEBUG");
    info("your logging level is DEBUG or INFO");
    notice("your logging level is DEBUG, INFO, or NOTICE");
    warning("your logging level is WARNING, INFO, NOTICE or DEBUG");
    error("your logging level is ERROR,  WARNING, INFO, NOTICE or DEBUG");
    critical("your logging level is CRITICAL, ERROR, WARNING, INFO, NOTICE or DEBUG");
    puts("");


    fclose(fptr);
    return 0;
}